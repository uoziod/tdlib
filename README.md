# TDLib in Docker

Gitlab pipelines that build [TDLib (Telegram Database Library)](https://github.com/tdlib/td) for a handful of platforms.

Each branch produces and publishes to Docker Hub a multi-arch (linux/amd64, linux/arm64 and linux/arm) image with a handful of TDLib versions (see [branches section](https://gitlab.com/uoziod/tdlib/-/branches/all) or [Docker Hub tags](https://hub.docker.com/r/uoziod/tdlib/tags) for available options). Feel free to contribute or reach out if you're looking for some specific combination. 


## Usage example

Dockerfile for a NodeJS app that uses TDLib:

```dockerfile
FROM uoziod/tdlib:1.8.22_alpine-3.19


# Install NodeJS

RUN apk add --update nodejs npm


# Cache node_modules

ADD package.json /tmp/package.json
RUN cd /tmp && npm i
RUN mkdir -p /opt/app && cp -a /tmp/node_modules /opt/app/


WORKDIR /opt/app
ADD . /opt/app


CMD ["npm", "start"]
```